from django.apps import AppConfig


class OneShotAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'one_shot_app'
